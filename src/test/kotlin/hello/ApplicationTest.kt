package hello

import org.junit.Assert.*
import org.junit.Test
import org.springframework.boot.test.context.SpringBootTest
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.test.context.junit.jupiter.SpringExtension

@ExtendWith(SpringExtension::class)
@SpringBootTest
class BlogApplicationTests {

    @Test
    fun contextLoads() {
        assertTrue(true)
    }

}
