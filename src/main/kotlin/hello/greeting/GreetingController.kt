package hello.greeting

import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.util.concurrent.atomic.AtomicLong

@RestController
class GreetingController {

    companion object {
        const val greetingTemplate = "Hello, %s!"
    }

    private val atomicCounter = AtomicLong()

    @RequestMapping("/greeting")
    fun greeting(@RequestParam(value = "name", defaultValue = "World") name: String) =
        Greeting(
            atomicCounter.incrementAndGet(),
            greetingTemplate.format(name)
        )
}
