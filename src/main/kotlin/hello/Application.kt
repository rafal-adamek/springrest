package hello

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class Application {
//    fun main(args : Array<String>) {
//        SpringApplication.run(Application::class.java, *args)
//    }
}

fun main(args: Array<String>) {
    runApplication<Application>(*args)
}
